import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hotel_booking/app_router.dart';
import 'package:hotel_booking/constants.dart';
import 'package:hotel_booking/controllers/MenuController.dart';
import 'package:hotel_booking/screens/login_screen.dart';
import 'package:hotel_booking/screens/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hotel_booking/web_cliente/web/main_page.dart' as WebMainPage;

import 'package:provider/provider.dart';

import 'generated/l10n.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => MenuController()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        onGenerateRoute: AppRouter.router.generator,
        title: 'Flutter hotel_booking Panel',
        theme: ThemeData.dark().copyWith(
          scaffoldBackgroundColor: bgColor,
          textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
              .apply(bodyColor: Colors.white),
          canvasColor: secondaryColor,
        ),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          S.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
        home:
            // WebMainPage.MainPage()
            // MainScreen()
            LoginPage(),
      ),
    );

    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   title: 'Flutter hotel_booking Panel',
    //   theme: ThemeData.dark().copyWith(
    //     scaffoldBackgroundColor: bgColor,
    //     textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
    //         .apply(bodyColor: Colors.white),
    //     canvasColor: secondaryColor,
    //   ),
    //   home: MultiProvider(providers: [
    //     ChangeNotifierProvider(
    //       create: (context) => MenuController(),
    //     ),
    //   ], child: LoginPage()
    //       //  MainScreen(),
    //       ),
    // );
  }
}
