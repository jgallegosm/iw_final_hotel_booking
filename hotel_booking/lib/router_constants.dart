import 'package:fluro/fluro.dart';
import 'package:hotel_booking/screens/login_screen.dart';
import 'package:hotel_booking/screens/registro_screen.dart';

class AppRoutes {
  // static final routeNotFoundHandler = Handler(handlerFunc: (context, params) {
  //   return EmpresaView(codigo: 2);
  // });

  static final registro =
      AppRoute('/registro/', Handler(handlerFunc: (context, params) {
    return RegistrationPage();
  }));

  static final login =
      AppRoute('/login/', Handler(handlerFunc: (context, params) {
    return LoginPage();
  }));

  // static final avanceAppRoute = AppRoute('/pasos/:id', Handler(
  //   handlerFunc: (context, params) {
  //     return PasoAPasoView(codigo: int.parse(params["id"]![0]));
  //   },
  // ));

  // static final ubicacion = AppRoute('/ubicacion/:id/:lat/:long', Handler(
  //   handlerFunc: (context, params) {
  //     return UbicacionView(
  //       id: int.parse(params["id"]![0]),
  //       long: double.parse(params["lat"]![0]),
  //       lat: double.parse(params["long"]![0]),
  //     );
  //   },
  // ));

  static final List<AppRoute> routes = [
    registro,
    login,
  ];
}
