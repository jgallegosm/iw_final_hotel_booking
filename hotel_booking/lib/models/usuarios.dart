// To parse this JSON data, do
//
//     final usuario = usuarioFromJson(jsonString);

import 'dart:convert';

Usuario usuarioFromJson(String str) => Usuario.fromJson(json.decode(str));

String usuarioToJson(Usuario data) => json.encode(data.toJson());

class Usuario {
  Usuario({
    this.lastName,
    this.email,
    this.rol,
    this.name,
    this.password,
    this.enabled,
    this.id,
  });

  String lastName;
  String email;
  String rol;
  String name;
  String password;
  bool enabled;
  String id;

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        lastName: json["last_name"],
        email: json["email"],
        rol: json["rol"],
        name: json["name"],
        password: json["password"],
        enabled: json["enabled"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "last_name": lastName,
        "email": email,
        "rol": rol,
        "name": name,
        "password": password,
        "enabled": enabled,
        "id": id,
      };
}
