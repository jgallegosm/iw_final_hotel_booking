import 'dart:io';

import 'package:another_flushbar/flushbar.dart';

import 'package:flutter/material.dart';
import 'package:hotel_booking/generated/l10n.dart';

import 'package:logger/logger.dart';
import 'package:provider/provider.dart';

class PerfilScreen extends StatefulWidget {
  @override
  _PerfilScreenState createState() => _PerfilScreenState();
}

class _PerfilScreenState extends State<PerfilScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool editPwd = false;

  dynamic _pickImageError;

  TextEditingController txtNombre = new TextEditingController();
  TextEditingController txtApellido = new TextEditingController();
  TextEditingController txtCorreo = new TextEditingController();
  TextEditingController txtPassword = new TextEditingController();
  final logger = Logger();

  final kBoxDecorationStyle = BoxDecoration(
    borderRadius: BorderRadius.circular(10.0),
    border: Border.all(
      color: Colors.white,
      width: 2,
    ),
  );

  Widget _buildFoto(BuildContext context) {
    return GestureDetector(
      onTap: () async {},
      child: Container(
        width: 150,
        height: 150,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: AssetImage("assets/images/profile_pic.png"),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget _buildBox(
      String nombre, Icon icono, TextEditingController controlador) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          nombre,
          style: TextStyle(
            color: Color(0xFF537445),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          child: TextField(
            controller: controlador,
            style: TextStyle(color: Colors.black87),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14, right: 10),
              prefixIcon: icono,
            ),
          ),
        ),
        SizedBox(
          height: 15,
        )
      ],
    );
  }

  Future setDatos() {
    // txtNombre.text = SessionHelper().firstName;
    // txtApellido.text = SessionHelper().lastName;
    // txtCorreo.text = SessionHelper().email;
    return null;
  }

  @override
  void initState() {
    // TODO: implement initState
    setDatos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          Positioned(
            child: Row(
              children: [
                OutlineButton(
                  color: Color(0x00000000),
                  child: Text("Inglés"),
                  onPressed: () {
                    setState(() {
                      S.load(Locale("EN"));
                    });
                    print("CAMBIADO A EN");
                  },
                ),
                OutlineButton(
                  color: Color(0x00000000),
                  child: Text("Español"),
                  onPressed: () {
                    setState(() {
                      S.load(Locale("ES"));
                    });
                    print("CAMBIADO A ES");
                  },
                ),
                OutlineButton(
                  color: Color(0x00000000),
                  child: Text("Portugues"),
                  onPressed: () {
                    setState(() {
                      S.load(Locale("PT"));
                    });
                    print("CAMBIADO A PT");
                  },
                ),
              ],
            ),
          ),
          Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width / 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _buildFoto(context),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    S.current.misDatos,
                    style: TextStyle(
                      color: Color(0xFF537445),
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  _buildBox(S.current.name,
                      Icon(Icons.people, color: Colors.lightGreen), txtNombre),
                  _buildBox(
                      S.current.Lastname,
                      Icon(Icons.people, color: Colors.lightGreen),
                      txtApellido),
                  _buildBox(
                      S.current.email,
                      Icon(Icons.alternate_email, color: Colors.lightGreen),
                      txtCorreo),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.current.email,
                        style: TextStyle(
                          color: Color(0xFF537445),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        decoration: kBoxDecorationStyle,
                        height: 60,
                        child: TextField(
                          onChanged: (text) {
                            editPwd = true;
                          },
                          controller: txtPassword,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(top: 14, right: 10),
                            prefixIcon: Icon(
                              Icons.lock_sharp,
                              color: Colors.lightGreen,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
