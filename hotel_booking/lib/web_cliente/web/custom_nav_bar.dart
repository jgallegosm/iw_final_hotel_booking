import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking/web_cliente/web/model_web_usuarios/menu.dart';
import 'package:hotel_booking/web_cliente/web/nav_item.dart';
import 'package:hotel_booking/widgets/snackbar_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomNavBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('menus');

  CustomNavBar({@required this.title});
  Future<void> _launchUniversalLinkIos(String url) async {
    if (await canLaunch(url)) {
      final bool nativeAppLaunchSucceeded = await launch(
        url,
        forceSafariVC: false,
        universalLinksOnly: true,
      );
      if (!nativeAppLaunchSucceeded) {
        await launch(url, forceSafariVC: true);
      }
    }
  }

  List<Menu> _menus = [
    Menu(inApp: true, tittle: "Perfil", url: "perfil"),
    // Menu(
    //     inApp: false,
    //     tittle: "Train",
    //     url:
    //         "https://stackoverflow.com/questions/24229290/how-do-use-a-switch-case-statement-in-dart"),
    // Menu(
    //     inApp: false,
    //     tittle: "Bus",
    //     url:
    //         "https://www.google.com/search?client=opera-gx&q=switch+case+flutter&sourceid=opera&ie=UTF-8&oe=UTF-8"),
    // Menu(
    //     inApp: false,
    //     tittle: "Airlines",
    //     url: "https://es.wikipedia.org/wiki/Casa"),
    // Menu(
    //     inApp: false,
    //     tittle: "otros",
    //     url:
    //         "https://www.google.com/search?client=opera-gx&q=hola&sourceid=opera&ie=UTF-8&oe=UTF-8"),
  ];

  Widget menuBuilder(List<Menu> aux) {
    aux.forEach((element) {
      Expanded(
        child: NavItem(
          title: aux[0].tittle,
        ),
      );
    });
    // return Row(
    //   children: [
    //     Expanded(
    //         child: NavItem(
    //       title: aux[0].tittle,
    //     ))
    //   ],
    // );
  }

  List<Widget> _rowList(List<Menu> aux) {
    List<Widget> elements = [];
    aux.forEach((element) {
      elements.add(
        Expanded(
            child: NavItem(
          title: element.tittle,
          inApp: element.inApp,
          url: element.url,
        )),
      );
    });
    return elements;
  }

  getMenus(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    print("------------------------------");
    print(snapshot.data.docs.length);
    return snapshot.data.docs
        .map((doc) => Expanded(
                    child: NavItem(
                  title: doc["tittle"],
                  inApp: doc["inApp"],
                  url: doc["url"],
                  enabled: doc["enabled"],
                ))

            // _menus.add(
            //           Menu(
            //             tittle: doc["tittle"],
            //             inApp: doc["inApp"],
            //             url: doc["url"],
            //           ),
            //         )

            // new ListTile(
            //   leading: Icon(Icons.title),
            //   title: new Row(
            //     children: [
            //       Text(doc["name"]),
            //       Text("  -  "),
            //       Text(doc["email"]),
            //       Text("  -  "),
            //       Text(doc["rol"]),
            //     ],
            //   ),
            //   subtitle: new Text(doc["last_name"].toString()),
            //   trailing:
            //       doc["enabled"] == false ? Icon(Icons.close) : Icon(Icons.check),
            //   onTap: () {
            //     if (doc["enabled"] == true) {
            //       firestore
            //           .collection("usuarios")
            //           .doc(doc["id"])
            //           .update({"enabled": false}).then((_) {
            //         snackBar3Sec(context, "usuario inhabilitado");
            //       });
            //     } else {
            //       firestore
            //           .collection("usuarios")
            //           .doc(doc["id"])
            //           .update({"enabled": true}).then((_) {
            //         snackBar3Sec(context, "usuario habilitado");
            //       });
            //     }
            //   },
            // ),
            )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 120.0, right: 120.0, top: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: Image(
                      image: AssetImage("assets/images/logo.png"),
                    ),
                  ),
                  Text(
                    "Fran´s Hotel",
                    style: Theme.of(context).textTheme.headline3,
                  )
                ],
              ),
            ),
          ),
          StreamBuilder(
            stream: firestore.collection("menus").snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) return new Text("Cargando menus");
              return Expanded(
                  child: Row(children: getMenus(context, snapshot)));
            },
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(52);
}
