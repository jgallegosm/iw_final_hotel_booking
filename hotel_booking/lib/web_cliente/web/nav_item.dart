import 'package:flutter/material.dart';
import 'package:hotel_booking/web_cliente/mobile/hotel_booking/hotel_app_theme.dart';
import 'package:hotel_booking/web_cliente/web/model_web_usuarios/menu.dart';
import 'package:hotel_booking/web_cliente/web/screens_web_view/perfil_view.dart';
import 'package:url_launcher/url_launcher.dart';

class NavItem extends StatefulWidget {
  final String title;
  final bool isSelected;
  final bool inApp;
  final String url;
  final bool enabled;

  NavItem(
      {@required this.title,
      this.isSelected = false,
      this.inApp,
      this.url,
      this.enabled});

  @override
  _NavItemState createState() => _NavItemState();
}

class _NavItemState extends State<NavItem> {
  bool hovered = false;

  Future<void> _launchUniversalLinkIos(String url) async {
    if (await canLaunch(url)) {
      final bool nativeAppLaunchSucceeded = await launch(
        url,
        forceSafariVC: false,
        universalLinksOnly: true,
      );
      if (!nativeAppLaunchSucceeded) {
        await launch(url, forceSafariVC: true);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(
        fontSize: 24.0, color: hovered ? Colors.white : Colors.black54);

    return InkWell(
      onHover: (hover) {
        setState(() {
          this.hovered = hover;
        });
      },
      onTap: () {
        if (widget.inApp == true) {
          switch (widget.url) {
            case "perfil":
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PerfilScreen(),
                  ));
              break;
          }
        } else {
          _launchUniversalLinkIos(widget.url);
        }
      },
      child: Container(
        padding: EdgeInsets.only(top: 8, bottom: 8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: hovered
                ? HotelAppTheme.buildLightTheme().primaryColor
                : Colors.transparent),
        child: Center(
          child: Text(
            widget.title,
            style: style,
          ),
        ),
      ),
    );
  }
}
