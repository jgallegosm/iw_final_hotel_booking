import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking/screens/login_screen.dart';

void popUpnUevoMenu(
  BuildContext context,
) {
  TextEditingController txtTTittle = TextEditingController();
  TextEditingController txtId = TextEditingController();
  bool inApp = false;
  bool enabled = false;
  TextEditingController txtUrl = TextEditingController();
  showDialog(
    context: context,
    builder: (_) => AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(32.0),
        ),
      ),
      title: Center(child: Text("Registrar")),
      elevation: 24,
      content: popNuevoMenu(
        txtTTittle,
        txtId,
        inApp,
        enabled,
        txtUrl,
      ),
    ),
  );
}

CollectionReference menus = FirebaseFirestore.instance.collection('menus');

Widget popNuevoMenu(
  TextEditingController txtTittle,
  TextEditingController txtId,
  bool inApp,
  bool enabled,
  TextEditingController txtUrl,
) {
  bool _enabled = false;
  return Container(
    width: 500,
    decoration: BoxDecoration(
        gradient:
            LinearGradient(colors: [Colors.blue, Colors.indigo.shade600])),
    child:
        //  authProvider.status == Status.Authenticating? Loading() :

        Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey[400],
                    offset: Offset(0, 3),
                    blurRadius: 24)
              ]),
          height: 420,
          child: ListView(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Agregar menu",
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(color: Colors.grey[400]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: TextField(
                      controller: txtTittle,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Titulo',
                          icon: Icon(Icons.person_outline)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(color: Colors.grey[400]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: TextField(
                      controller: txtId,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'id',
                          icon: Icon(Icons.email_outlined)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(color: Colors.grey[400]),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: TextField(
                      controller: txtUrl,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Url',
                          icon: Icon(Icons.lock_open)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                color: Colors.grey,
                child: Row(
                  children: [
                    Text(
                      "habilitado",
                      style: TextStyle(color: Colors.black),
                    ),
                    Checkbox(
                        activeColor: Colors.black,
                        focusColor: Colors.blue,
                        checkColor: Colors.indigo,
                        value: _enabled,
                        onChanged: (bool value) {
                          if (value == true) {
                            _enabled = true;
                          } else {
                            _enabled = false;
                          }
                          print(value);
                        })
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),

              // Padding(
              //     padding: const EdgeInsets.symmetric(horizontal: 20),
              //     child: CheckboxListTile(
              //       title: Text("En la aplicación"),
              //       value: inApp,
              //       onChanged: (bool value) {
              //         inApp = value;
              //       },
              //     )),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(color: Colors.indigo),
                  child: FlatButton(
                    onPressed: () async {
                      menus.doc(txtId.text).set({
                        "tittle": txtTittle.text,
                        "id": txtId.text,
                        "url": txtUrl.text,
                        "enabled": true,
                        "inApp": false,
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "REGISTRAR",
                            style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
