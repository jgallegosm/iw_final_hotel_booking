import 'dart:html';
import 'dart:js';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking/screens/login_screen.dart';

void popUpRegistro(
  BuildContext context,
) {
  TextEditingController txtNombre = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtPwd = TextEditingController();

  showDialog(
    context: context,
    builder: (_) => AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(32.0),
        ),
      ),
      title: Center(child: Text("Registrar")),
      elevation: 24,
      content: popRegistro(txtNombre, txtEmail, txtPwd),
    ),
  );
}

Future<String> signUp(String correo, String contrasena) async {
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  UserCredential credential = await firebaseAuth.createUserWithEmailAndPassword(
      email: correo, password: contrasena);
  return credential.user.uid;
}

CollectionReference usuarios =
    FirebaseFirestore.instance.collection('usuarios');

Widget popRegistro(
  TextEditingController txtNombre,
  TextEditingController txtEmail,
  TextEditingController txtPwd,
) {
  return Container(
    decoration: BoxDecoration(
        gradient:
            LinearGradient(colors: [Colors.blue, Colors.indigo.shade600])),
    child:
        //  authProvider.status == Status.Authenticating? Loading() :

        Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          color: Colors.red,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey[400],
                      offset: Offset(0, 3),
                      blurRadius: 24)
                ]),
            height: 420,
            width: 350,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "REGISTRATE",
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    decoration: BoxDecoration(color: Colors.grey[400]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: TextField(
                        controller: txtNombre,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Username',
                            icon: Icon(Icons.person_outline)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    decoration: BoxDecoration(color: Colors.grey[400]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: TextField(
                        controller: txtEmail,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'email',
                            icon: Icon(Icons.email_outlined)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    decoration: BoxDecoration(color: Colors.grey[400]),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: TextField(
                        controller: txtPwd,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Password',
                            icon: Icon(Icons.lock_open)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                    decoration: BoxDecoration(color: Colors.indigo),
                    child: FlatButton(
                      onPressed: () async {
                        String uid = await signUp(
                            txtEmail.text.toString(), txtPwd.text.toString());
                        if (uid.isNotEmpty) {
                          usuarios.doc(uid).set({
                            "name": txtNombre.text,
                            "last_name": txtNombre.text,
                            "email": txtEmail.text,
                            "enabled": true,
                            "rol": "admin",
                            "id": uid.toString(),
                            "password": txtPwd.text,
                          });
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "REGISTRAR",
                              style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}
