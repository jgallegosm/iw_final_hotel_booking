// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Lastname" : MessageLookupByLibrary.simpleMessage("Lastname"),
    "Password" : MessageLookupByLibrary.simpleMessage("Password"),
    "cuenta" : MessageLookupByLibrary.simpleMessage("You do not have an account"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "forgotPassword" : MessageLookupByLibrary.simpleMessage("Forgot your password?"),
    "login" : MessageLookupByLibrary.simpleMessage("LOGIN"),
    "loginButton" : MessageLookupByLibrary.simpleMessage("Login"),
    "loginOption" : MessageLookupByLibrary.simpleMessage("LOgin here"),
    "misDatos" : MessageLookupByLibrary.simpleMessage("My info"),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "signUp" : MessageLookupByLibrary.simpleMessage("SIGN UP"),
    "signUpButton" : MessageLookupByLibrary.simpleMessage("sign up"),
    "signUpOption" : MessageLookupByLibrary.simpleMessage("Sign up"),
    "username" : MessageLookupByLibrary.simpleMessage("Username"),
    "yaTienesCuente" : MessageLookupByLibrary.simpleMessage("Do you have a account?")
  };
}
