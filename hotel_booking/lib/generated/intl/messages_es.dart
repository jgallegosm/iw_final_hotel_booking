// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Lastname" : MessageLookupByLibrary.simpleMessage("Apellido"),
    "Password" : MessageLookupByLibrary.simpleMessage("Contraseña"),
    "cuenta" : MessageLookupByLibrary.simpleMessage("No tienes una cuenta"),
    "email" : MessageLookupByLibrary.simpleMessage("Correo electrónico"),
    "forgotPassword" : MessageLookupByLibrary.simpleMessage("¿Olvidó su contraseña?"),
    "login" : MessageLookupByLibrary.simpleMessage("INICIAR SESIÓN"),
    "loginButton" : MessageLookupByLibrary.simpleMessage("Iniciar sesión"),
    "loginOption" : MessageLookupByLibrary.simpleMessage("INICIAR SESIÓN aquí"),
    "misDatos" : MessageLookupByLibrary.simpleMessage("Mi información"),
    "name" : MessageLookupByLibrary.simpleMessage("Nombre"),
    "signUp" : MessageLookupByLibrary.simpleMessage("REGÍSTRESE"),
    "signUpButton" : MessageLookupByLibrary.simpleMessage("Registrarse"),
    "signUpOption" : MessageLookupByLibrary.simpleMessage("Registrarse"),
    "username" : MessageLookupByLibrary.simpleMessage("Nombre de usuario"),
    "yaTienesCuente" : MessageLookupByLibrary.simpleMessage("¿Tienes cuenta?")
  };
}
