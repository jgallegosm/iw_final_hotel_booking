// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pt locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pt';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Lastname" : MessageLookupByLibrary.simpleMessage("Sobrenome"),
    "Password" : MessageLookupByLibrary.simpleMessage("Senha"),
    "cuenta" : MessageLookupByLibrary.simpleMessage("Você não tem uma conta"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "forgotPassword" : MessageLookupByLibrary.simpleMessage("Esqueceu sua senha?"),
    "login" : MessageLookupByLibrary.simpleMessage("INICIAR SESSÃO"),
    "loginButton" : MessageLookupByLibrary.simpleMessage("Login"),
    "loginOption" : MessageLookupByLibrary.simpleMessage("LOgin aqui"),
    "misDatos" : MessageLookupByLibrary.simpleMessage("minha informação"),
    "name" : MessageLookupByLibrary.simpleMessage("Nome"),
    "signUp" : MessageLookupByLibrary.simpleMessage("Registar-se"),
    "signUpButton" : MessageLookupByLibrary.simpleMessage("Registre-se"),
    "signUpOption" : MessageLookupByLibrary.simpleMessage("Inscreva-se"),
    "username" : MessageLookupByLibrary.simpleMessage("Nome de usuário"),
    "yaTienesCuente" : MessageLookupByLibrary.simpleMessage("Você tem uma conta?")
  };
}
