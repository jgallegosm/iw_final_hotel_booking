import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:hotel_booking/constants.dart';
import 'package:hotel_booking/controllers/MenuController.dart';
import 'package:hotel_booking/responsive.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking/screens/dashboard/components/recent_files.dart';
import 'package:hotel_booking/widgets/popup_register.dart';
import 'package:hotel_booking/widgets/snackbar_widget.dart';

import 'dashboard/components/header.dart';
import 'main/components/side_menu.dart';

import 'package:provider/provider.dart';

class UsuariosView extends StatefulWidget {
  @override
  _UsuariosViewState createState() => _UsuariosViewState();
}

class _UsuariosViewState extends State<UsuariosView> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('usuarios');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideMenu(),
      appBar: AppBar(),
      body: SafeArea(
        child: Center(
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Column(
              children: [
                StreamBuilder<QuerySnapshot>(
                  stream: firestore.collection("usuarios").snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData) return new Text("No hay usuarios");
                    return Expanded(
                        child: ListView(children: getUsers(snapshot)));
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                RaisedButton(
                  onPressed: () {
                    popUpRegistro(context);
                  },
                  child: Text("nuevo usuario"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getUsers(AsyncSnapshot<QuerySnapshot> snapshot) {
    print("------------------------------");
    print(snapshot.data.docs.length);
    return snapshot.data.docs
        .map(
          (doc) => new ListTile(
            leading: Icon(Icons.title),
            title: new Row(
              children: [
                Text(doc["name"]),
                Text("  -  "),
                Text(doc["email"]),
                Text("  -  "),
                Text(doc["rol"]),
              ],
            ),
            subtitle: new Text(doc["last_name"].toString()),
            trailing:
                doc["enabled"] == false ? Icon(Icons.close) : Icon(Icons.check),
            onTap: () {
              if (doc["enabled"] == true) {
                firestore
                    .collection("usuarios")
                    .doc(doc["id"])
                    .update({"enabled": false}).then((_) {
                  snackBar3Sec(context, "usuario inhabilitado");
                });
              } else {
                firestore
                    .collection("usuarios")
                    .doc(doc["id"])
                    .update({"enabled": true}).then((_) {
                  snackBar3Sec(context, "usuario habilitado");
                });
              }
            },
          ),
        )
        .toList();
  }
}

// DataRow userRowuserRow(RecentFile fileInfo) {
//   return DataRow(
//     cells: [
//       DataCell(
//         Row(
//           children: [
//             SvgPicture.asset(
//               fileInfo.icon,
//               height: 30,
//               width: 30,
//             ),
//             Padding(
//               padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
//               child: Text(fileInfo.title),
//             ),
//           ],
//         ),
//       ),
//       DataCell(Text(fileInfo.date)),
//       DataCell(Text(fileInfo.size)),
//     ],
//   );
// }

// class UsuariosItems extends StatelessWidget {
//   const UsuariosItems({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(defaultPadding),
//       decoration: BoxDecoration(
//         color: secondaryColor,
//         borderRadius: const BorderRadius.all(Radius.circular(10)),
//       ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Text(
//             "Recent Files",
//             style: Theme.of(context).textTheme.subtitle1,
//           ),
//           SizedBox(
//             width: double.infinity,
//             child: DataTable2(
//               columnSpacing: defaultPadding,
//               minWidth: 600,
//               columns: [
//                 DataColumn(
//                   label: Text("Nombre"),
//                 ),
//                 DataColumn(
//                   label: Text("Correo"),
//                 ),
//                 DataColumn(
//                   label: Text("rol"),
//                 ),
//               ],
//               rows: List.generate(
//                 demoRecentFiles.length,
//                 (index) => userRow(demoRecentFiles[index]),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
