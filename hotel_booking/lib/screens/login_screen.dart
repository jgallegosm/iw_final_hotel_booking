import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking/app_router.dart';
import 'package:hotel_booking/generated/l10n.dart';
import 'package:hotel_booking/screens/main/main_screen.dart';
import 'package:hotel_booking/screens/registro_screen.dart';
import 'package:hotel_booking/web_cliente/mobile/main_page.dart';
import 'package:hotel_booking/widgets/snackbar_widget.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:hotel_booking/web_cliente/web/main_page.dart' as WebMainPage;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  TextEditingController txtEmail = TextEditingController();

  TextEditingController txtPwd = TextEditingController();

  Logger logger;

  Future<String> logIn(String email, String password) async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    try {
      UserCredential credential = await firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);

      print(credential.user.uid);
      return credential.user.uid;
    } on FirebaseAuthException catch (e) {
      print("--------------------");
      logger.e(e.message);
      logger.e(e.code);
      print(e.code);
    }
  }

  final firestoreInstance = FirebaseFirestore.instance;

  bool status;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: [Colors.blue, Colors.indigo[900]])),
      // child: authProvider.status == Status.Authenticating? Loading() :
      child: Scaffold(
        // floatingActionButton:
        key: _key,
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Positioned(
              child: Row(
                children: [
                  OutlineButton(
                    color: Color(0x00000000),
                    child: Text("Inglés"),
                    onPressed: () {
                      setState(() {
                        S.load(Locale("EN"));
                      });
                      print("CAMBIADO A EN");
                    },
                  ),
                  OutlineButton(
                    color: Color(0x00000000),
                    child: Text("Español"),
                    onPressed: () {
                      setState(() {
                        S.load(Locale("ES"));
                      });
                      print("CAMBIADO A ES");
                    },
                  ),
                  OutlineButton(
                    color: Color(0x00000000),
                    child: Text("Portugues"),
                    onPressed: () {
                      setState(() {
                        S.load(Locale("PT"));
                      });
                      print("CAMBIADO A PT");
                    },
                  ),
                ],
              ),
            ),
            Center(
              child: Container(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey,
                            offset: Offset(0, 3),
                            blurRadius: 24)
                      ]),
                  height: 400,
                  width: 350,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        S.current.login,
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          decoration: BoxDecoration(color: Colors.grey[400]),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: TextField(
                              controller: txtEmail,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: S.current.email,
                                  icon: Icon(Icons.email_outlined)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          decoration: BoxDecoration(color: Colors.grey[400]),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: TextField(
                              controller: txtPwd,
                              obscureText: true,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: S.current.Password,
                                  icon: Icon(Icons.lock_open)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              S.current.signUpOption,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          decoration: BoxDecoration(color: Colors.indigo),
                          child: FlatButton(
                            onPressed: () async {
                              String uid =
                                  await logIn(txtEmail.text, txtPwd.text);
                              if (uid.isNotEmpty) {
                                firestoreInstance
                                    .collection("usuarios")
                                    .doc(uid)
                                    .get()
                                    .then((value) {
                                  print(value.data()["id"]);
                                  print(value.data()["email"]);
                                  print(value.data()["rol"]);

                                  if (value.data()["enabled"] == true) {
                                    if (value.data()["rol"] == "admin") {
                                      if (FirebaseAuth.instance.currentUser
                                              .emailVerified ==
                                          true) {
                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  MainScreen(),
                                            ));
                                      } else {
                                        snackBar3Sec(context,
                                            "VERIFIQUE LA BANDEJA DE ENTRADA DE SU CORREO");
                                      }
                                    } else {
                                      if (FirebaseAuth.instance.currentUser
                                              .emailVerified ==
                                          true) {
                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  WebMainPage.MainPage(),
                                            ));
                                      } else {
                                        snackBar3Sec(context,
                                            "VERIFIQUE LA BANDEJA DE ENTRADA DE SU CORREO");
                                      }
                                    }
                                  } else {
                                    snackBar3Sec(
                                        context, "Comunicate con el admin");
                                  }
                                });
                              } else {
                                snackBar3Sec(
                                    context, "No se inicio sesión verifique");
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 4),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    S.current.loginButton,
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              S.current.cuenta,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.grey),
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: 35,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RegistrationPage()),
                                    );
                                  },
                                  child: Text(
                                    S.current.signUpOption,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.indigo),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
