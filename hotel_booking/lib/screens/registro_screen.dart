import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking/generated/l10n.dart';
import 'package:hotel_booking/screens/login_screen.dart';
import 'package:hotel_booking/widgets/snackbar_widget.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  TextEditingController txtNombre = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtPws = TextEditingController();
  Logger logger = Logger();

  Future<String> signUp(String correo, String contrasena) async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    UserCredential credential = await firebaseAuth
        .createUserWithEmailAndPassword(email: correo, password: contrasena);
    return credential.user.uid;
  }

  Future<String> logIn(String email, String password) async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    try {
      UserCredential credential = await firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);

      print(credential.user.uid);
      return credential.user.uid;
    } on FirebaseAuthException catch (e) {
      print("--------------------");
      logger.e(e.message);
      logger.e(e.code);
      print(e.code);
    }
  }

  CollectionReference usuarios =
      FirebaseFirestore.instance.collection('usuarios');

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient:
              LinearGradient(colors: [Colors.blue, Colors.indigo.shade600])),
      child:
          //  authProvider.status == Status.Authenticating? Loading() :

          Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Positioned(
              child: Row(
                children: [
                  OutlineButton(
                    color: Color(0x00000000),
                    child: Text("Inglés"),
                    onPressed: () {
                      setState(() {
                        S.load(Locale("EN"));
                      });
                      print("CAMBIADO A EN");
                    },
                  ),
                  OutlineButton(
                    color: Color(0x00000000),
                    child: Text("Español"),
                    onPressed: () {
                      setState(() {
                        S.load(Locale("ES"));
                      });
                      print("CAMBIADO A ES");
                    },
                  ),
                  OutlineButton(
                    color: Color(0x00000000),
                    child: Text("Portugues"),
                    onPressed: () {
                      setState(() {
                        S.load(Locale("PT"));
                      });
                      print("CAMBIADO A PT");
                    },
                  ),
                ],
              ),
            ),
            Center(
              child: Container(
                color: Colors.red,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey[400],
                            offset: Offset(0, 3),
                            blurRadius: 24)
                      ]),
                  height: 420,
                  width: 350,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        S.current.signUp,
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          decoration: BoxDecoration(color: Colors.grey[400]),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: TextField(
                              controller: txtNombre,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: S.current.username,
                                  icon: Icon(Icons.person_outline)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          decoration: BoxDecoration(color: Colors.grey[400]),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: TextField(
                              controller: txtEmail,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: S.current.email,
                                  icon: Icon(Icons.email_outlined)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          decoration: BoxDecoration(color: Colors.grey[400]),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: TextField(
                              controller: txtPws,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: S.current.Password,
                                  icon: Icon(Icons.lock_open)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                          decoration: BoxDecoration(color: Colors.indigo),
                          child: FlatButton(
                            onPressed: () async {
                              String uid = await signUp(
                                  txtEmail.text.toString(),
                                  txtPws.text.toString());
                              if (uid.isNotEmpty) {
                                usuarios.doc(uid).set({
                                  "name": txtNombre.text,
                                  "last_name": txtNombre.text,
                                  "email": txtEmail.text,
                                  "enabled": true,
                                  "rol": "cliente",
                                  "id": uid.toString(),
                                  "password": txtPws.text,
                                });
                                var user = FirebaseAuth.instance.currentUser;
                                Navigator.pop(context);
                                user.sendEmailVerification().then((value) {
                                  snackBar3Sec(context,
                                      "VERIFIQUE SU CORREO ELECTRÓNICO");
                                });
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 4),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    S.current.signUpButton,
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              S.current.yaTienesCuente,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: 30,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LoginPage()),
                                    );
                                  },
                                  child: Text(
                                    S.current.loginOption,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.indigo),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
