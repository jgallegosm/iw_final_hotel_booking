import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking/widgets/popup_new_menu.dart';
import 'package:hotel_booking/widgets/popup_register.dart';
import 'package:hotel_booking/widgets/snackbar_widget.dart';
import 'main/components/side_menu.dart';

class GestorMenusView extends StatefulWidget {
  @override
  _GestorMenusViewState createState() => _GestorMenusViewState();
}

class _GestorMenusViewState extends State<GestorMenusView> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('usuarios');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: RaisedButton(
        onPressed: () {
          popUpnUevoMenu(context);
        },
        child: Text("Nuevo Menu"),
      ),
      drawer: SideMenu(),
      appBar: AppBar(),
      body: SafeArea(
        child: Center(
          child: SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: Column(
              children: [
                StreamBuilder<QuerySnapshot>(
                  stream: firestore.collection("menus").snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData) return new Text("No hay menus");
                    return Expanded(
                        child: ListView(children: getUsers(snapshot)));
                  },
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getUsers(AsyncSnapshot<QuerySnapshot> snapshot) {
    print("------------------------------");
    print(snapshot.data.docs.length);
    return snapshot.data.docs
        .map(
          (doc) => new ListTile(
            leading: Icon(Icons.title),
            title: new Row(
              children: [
                Text(doc["tittle"]),
                Text("  -  "),
                Container(
                  child: Expanded(
                    child: Text(
                      doc["url"],
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Text("  -  "),
              ],
            ),
            subtitle: new Text(doc["url"].toString()),
            trailing:
                doc["enabled"] == false ? Icon(Icons.close) : Icon(Icons.check),
            onTap: () {
              if (doc["enabled"] == true) {
                firestore
                    .collection("menus")
                    .doc(doc["id"])
                    .update({"enabled": false}).then((_) {
                  snackBar3Sec(context, "usuario inhabilitado");
                });
              } else {
                firestore
                    .collection("menus")
                    .doc(doc["id"])
                    .update({"enabled": true}).then((_) {
                  snackBar3Sec(context, "usuario habilitado");
                });
              }
            },
          ),
        )
        .toList();
  }
}
